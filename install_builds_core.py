#!/usr/bin/env python3
# Run this script with apk and ipa builds in the "--builds" argument
# It will automatically detect devices, and install builds on them
# Dependencies: adb and libimobiledevice
# On Linux for iOS install libimobiledevice-utils and ideviceinstaller
# version: 1.0.1
import argparse
import asyncio
import os
import platform
import re
import sys
import time
from asyncio.subprocess import PIPE
from subprocess import list2cmdline

# for win & mac os download from https://github.com/libimobiledevice-win32/imobiledevice-net/releases
libimobiledevice = ""
if platform.system() == 'Windows': # Windows
    libimobiledevice = 'libimobiledevice.1.2.1-r929-win-x64\\'
elif platform.system() == 'Darwin': # Mac OS
    libimobiledevice = "libimobiledevice.1.2.1-r929-osx-x64/"

report = []
parallel = True

class AndroidDevice():
    @staticmethod
    async def get_connected_devices():
        cmd = "adb devices"
        device_ids = []

        p = await asyncio.create_subprocess_shell(cmd, stdout=PIPE)
        output, _ = await p.communicate()

        if p.returncode == 0:
            output = output.split(b'\n')[1:]
            device_ids = [x.split()[0] for x in output if x and not x.isspace()]
            device_ids = [x.decode() for x in device_ids]

        devices = []
        for device_id in device_ids:
            cmd = f"adb -s {device_id} shell getprop ro.product.model"
            p = await asyncio.create_subprocess_shell(cmd, stdout=PIPE, stderr=PIPE)
            stdout, _ = await p.communicate()

            if p.returncode == 0:
                device_name = stdout.decode().strip()
                devices.append((device_id, device_name))

        print(f"\nFound {len(devices)} Android devices")
        for index, (device_id, device_name) in enumerate(devices):
            print(f'  {index + 1}. name: "{device_name}" id: "{device_id}"')
        print("\n")

        return devices

    def __init__(self, deviceId, deviceName):
        self.id = deviceId
        self.name = deviceName

    async def _get_api_level(self):
        retcode, stdout, _ = await self._adb_shell("getprop ro.build.version.sdk")
        if retcode == 0:
            return int(stdout.decode().strip())
        else:
            raise Exception("Could not obtain the device's API level")

    async def _adb_run(self, *kvargs):
        command = list2cmdline(["adb", *kvargs])
        self._log("Execute: " + command)
        p = await asyncio.create_subprocess_shell(command, stdout=PIPE, stderr=PIPE)
        stdout, stderr = await p.communicate()
        output = stdout.decode("utf-8")
        # skip `[  3%] /data/local/tmp/build.apk` lines
        output = re.sub(r'(?m)^.*\[[ ]*\d+\%\].*\n?', '', output)
        self._log("output: " + output)
        return p.returncode, stdout, stderr

    async def _adb(self, cmd, *kvargs):
        return await self._adb_run('-s', self.id, cmd, *kvargs)

    async def _adb_shell(self, cmd):
        return await self._adb("shell", cmd)

    async def uninstall_app(self, app):
        await self._adb("uninstall", app)

    async def install_build(self, build):
        self._log(f"Installing {build} on {self.name}")
        kvargs = ["-r", "-d", "-t"]
        # enable to skip permission popups
        #if self._get_api_level() >= 23:
        #    kvargs.append("-g")
        kvargs.append(build)
        return await self._adb("install", *kvargs)

    def _log(self, message):
        print(f"[{self.id}] {message}")

class IosDevice():
    @staticmethod
    async def get_connected_devices():
        devices = []

        try:
            p = await asyncio.create_subprocess_exec(f"{libimobiledevice}idevice_id", "--list", stdout=PIPE)
            output, _ = await p.communicate()
            output = output.decode()
            output_lines = output.split('\n')
            output_lines = list(filter(None, output_lines))

            for device_id in output_lines:
                device_id = device_id.rstrip('\r\n')
                p = await asyncio.create_subprocess_exec(f"{libimobiledevice}idevice_id", f"{device_id}", stdout=PIPE)
                output, _ = await p.communicate()
                device_name = output.decode().split('\n')[0].rstrip('\r\n')
                devices.append((device_id, device_name))

            print(f"\nFound {len(devices)} iOS devices")
            for index, (device_id, device_name) in enumerate(devices):
                print(f'  {index + 1}. name: "{device_name}" id: "{device_id}"')
            print("\n")
        except FileNotFoundError as e:
            print("ERROR: You probably don't have libimobiledevice installed, iOS installation is impossible")
            return []
        except Exception as e:
            print(e)
            return []

        return devices

    def __init__(self, deviceId, deviceName):
        self.id = deviceId
        self.name = deviceName

    async def pair(self):
        self._log(f"Pairing with {self.id}")
        args = [f"{libimobiledevice}idevicepair", "-u", str(self.id), "pair"]
        self._log("Execute: " + list2cmdline(args))
        process = await asyncio.create_subprocess_exec(*args, stdout=PIPE)
        output, _ = await process.communicate()
        self._log("output: " + output.decode("utf-8"))

    async def install_build(self, build):
        self._log(f"Installing {build} on {self.name}")
        args = [f"{libimobiledevice}ideviceinstaller", "-u", str(self.id), "-i", str(build)]
        self._log("Execute: " + list2cmdline(args))
        process = await asyncio.create_subprocess_exec(*args, stdout=PIPE)
        lines = []
        while True:
            output = await process.stdout.readline()
            if not output:
                break
            output = output.decode("utf-8")
            lines.append(output)
            self._log(output.strip())
        return "\n".join(lines)

    def _log(self, message):
        print(f"[{self.id}] {message}")

async def HadleIpa(ipas, device_id, device_name):
    device = IosDevice(device_id, device_name)
    for build in ipas:
        await device.pair()
        output = await device.install_build(build)
        if "Install: Complete" in output:
            report.append(f"Successfully installed {build} on {device_name}")
        else:
            report.append(f"Failed to install {build} on {device_name} (check device first, it could be a false-positive and build is installed)")

async def handle_ios(ipas):
    devices = await IosDevice.get_connected_devices()
    if not ipas:
        return
    print("iOS builds to install:")
    for build in ipas:
        print(f"- {build}")

    if parallel:
        logger = None

        if len(devices) > 1 and platform.system() == 'Windows':
            # hackfix: spawn background logger to avoid "AFC Write error: 35" error on Windows
            # when spawning multiple installing processes
            FNULL = open(os.devnull, 'w')
            logger = await asyncio.create_subprocess_exec(f"{libimobiledevice}idevicesyslog", "-u", devices[1][0], stdout=FNULL)
            await asyncio.sleep(2)

        await asyncio.gather(*(HadleIpa(ipas, device_id, device_name) for device_id, device_name in devices))

        if logger:
            logger.terminate()
    else:
        for device_id, device_name in devices:
            await HadleIpa(ipas, device_id, device_name)

async def HandleApk(apks, device_id, device_name):
    device = AndroidDevice(device_id, device_name)
    await device.uninstall_app("com.huuuge.casino.slots")
    await device.uninstall_app("com.huuuge.casino.texas")
    await device.uninstall_app("com.huuuge.casino.slots.amazon")
    await device.uninstall_app("com.huuuge.casino.texas.amazon")
    for build in apks:
        _, output, output2 = await device.install_build(build)
        output = output.decode("utf-8")
        output2 = output2.decode("utf-8")
        if "Success" in output:
            report.append(f"Successfully installed {build} on {device_name}")
        else:
            report.append(f"Failed to install {build} on {device_name}: {output} {output2}")

async def handle_android(apks):
    devices = await AndroidDevice.get_connected_devices()
    if not apks:
        return
    print("Android builds to install " + ("(in parallel):" if parallel else ":"))
    for build in apks:
        print(f"- {build}")

    if parallel:
        await asyncio.gather(*(HandleApk(apks, device_id, device_name) for device_id, device_name in devices))
    else:
        for device_id, device_name in devices:
            await HandleApk(apks, device_id, device_name)

async def install_builds(builds):
    ipas = [arg for arg in builds if ".ipa" in arg]
    apks = [arg for arg in builds if ".apk" in arg]

    start = time.perf_counter()

    await asyncio.gather(handle_ios(ipas), handle_android(apks))

    print("\nreport:")
    for line in report:
        print(line)

    elapsed = time.perf_counter() - start
    print(f"Finished installing builds in {elapsed:0.2f} seconds.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Build installation tool. Run without arguments to print list of Android and iOS devices.')
    parser.add_argument('--parallel', '-p', help='Install builds in parallel', dest='parallel', action='store_true')
    parser.add_argument('--no-parallel', '-np', help='Install builds successively', dest='parallel', action='store_false')
    parser.set_defaults(parallel=True)
    parser.add_argument('--builds', '-b', help='list of builds to be installed',
                        nargs='*', default=[])
    args = parser.parse_args()
    parallel = args.parallel

    if sys.platform == 'win32':
        loop = asyncio.ProactorEventLoop()
        asyncio.set_event_loop(loop)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(install_builds(args.builds))
